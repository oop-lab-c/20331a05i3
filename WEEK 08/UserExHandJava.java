
class MyException extends Exception {
	public MyException(String s)
	{

		super(s);
	}
}
 class hello {

	public static void main(String args[])
	{
		try {
			throw new MyException("hello");
		}
		catch (MyException e) {
			System.out.println("caught");
			System.out.println(e.getMessage());
		}
        finally{
            System.out.println("finally block is always executed");
        }
	}
 }


