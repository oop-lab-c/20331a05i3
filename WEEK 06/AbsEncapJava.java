
class AbsEncapJava {
    private int priVar;
    protected int proVar;
    public int pubVar;

    public void setVar(int priValue, int proValue, int pubValue)
    {
        priVar = priValue;
        proVar = proValue;
        pubVar = pubValue;
    }

    public void getVar() 
    {
        System.out.println("priVar : "+priVar);
        System.out.println("proVar : "+proVar);
        System.out.println("pubVar : "+pubVar);
    }

    public static void main(String[] args) {
        AbsEncapJava obj = new AbsEncapJava();
        obj.setVar(2, 5, 9);
        obj.getVar();

    }
}
