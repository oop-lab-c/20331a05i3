#include<iostream>
using namespace std;
class AccessSpecifierDemo{
    private:
        int priVar;
    protected:
        int proVar;
    public:
      int pubVar;
    public:
    void setValue(int priValue,int proValue,int pubValue)
    {
        priVar = priValue;
        proVar = proValue;
        pubVar = pubValue;
    }
     public:
    void getVar()
    {
       cout<<"priVar : "<<priVar <<endl;
        cout<<"proVar : "<<proVar <<endl;
         cout<<"pubVar : "<<pubVar <<endl;
    }
};
int main()
{
     AccessSpecifierDemo obj;
     obj.setValue(7,8,9);
     obj.getVar();
     
     return 0;
}
