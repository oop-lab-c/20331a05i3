#include <iostream>    
using namespace std;    
class Sum 
{    
    public:    
	 int sum(int x,int y)
	{      
        return x + y;      
    }      
     int sum(int x, int y, int z)      
    {      
        return x + y + z;      
    }      
};     
int main(void) 
{    
    Sum obj;    
    cout<<"Sum of two number="<<obj.sum(1, 2)<<endl;      
    cout<<"Sum of three number="<<obj.sum(3, 4, 5);     
   return 0;    
}    