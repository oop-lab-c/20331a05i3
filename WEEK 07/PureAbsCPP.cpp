#include<iostream>
using namespace std;
class smartphone{
    public:
    virtual void takeselfie()=0; //virtual function
};
class android :public smartphone{
    public:
    void takeselfie(){
        cout<<"android selfie"<<endl;
    }

};
class apple :public smartphone{
    public:
    void takeselfie(){
        cout<<"ios selfie"<<endl;
    }    

};
int main(){
    android obj1;
    apple obj2;
    obj1.takeselfie();
    obj2.takeselfie();


}