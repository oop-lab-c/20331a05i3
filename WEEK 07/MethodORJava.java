
public class Overriding {
    public static void main(String[] args) {
        Derived obj = new Derived();
        obj.display();
    }
}
class Base{
    void display(){
        System.out.println("Base class");
    }
}
class Derived extends Base{
    void display(){
        System.out.println("Derived class");
    }
}
