#include<iostream>
using namespace std;

class Complex {
private:
	int real, imag;
public:
    Complex(){ 
    }
	Complex(int r, int i)
     {
      real = r;
      imag = i;
      }
	Complex operator + (Complex const &obj) {
		Complex res;
		res.real = real + obj.real;
		res.imag = imag + obj.imag;
		return res;
	}
	void print() { cout << real << " + " << imag <<"i"<< '\n'; }
};

int main()
{
	Complex c1(1, 5), c2(8, 9);
	Complex c3 = c1 + c2;
	c3.print();
}
