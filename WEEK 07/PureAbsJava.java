public class PureAbstraction {
    public static void main(String[] args) {
        derived obj = new derived();
        obj.show();
    }
}
interface base{
    
     void show();
}
class derived implements base{
    public void show(){
        System.out.println("hello");
    }
}
