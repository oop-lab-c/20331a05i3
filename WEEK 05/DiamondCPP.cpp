
#include<iostream>
using namespace std;
class Base{
    public:
    Base(){
        cout<<"Base class constructor called."<<endl;
    }
};
class Derived1 : public Base{
    public:
    Derived1(){
        cout<<"Derived 1 class constructor called."<<endl;
    }
};
class Derived2 : public Base{
    public:
    Derived2(){
        cout<<"Derived 2 class constructor called."<<endl;
    }
};
class Child : public Derived1,Derived2{
    public:
    Child(){
        cout<<"Child class constructor called."<<endl;
    }
};
int main()
{
    Child obj;
    return 0;
}