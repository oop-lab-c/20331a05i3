
#include<iostream>
using namespace std;
// base class
class base{
    
};
//single inheritance and Hierarchical Inheritance
class parent1 : public base{
    public:
    parent1(){
        cout<<"parent1 is inherited from base class"<<endl;
    }
};
class parent2 : public base{
    public:
    parent2(){ 
        cout<<"parent2 is inherited from base class"<<endl;
    }
};
//Multilevel Inheritance
class child1: public parent1{
    public:
    child1(){
        cout<<"child1 is inherited from parent1"<<endl;
    }
};
//Multiple inheritance
class child2: public parent1,parent2{
    public:
    child2(){
        cout<<"child2 is inherited from parent1 and parent2"<<endl;
    }
};
//Hybrid inheritance
class Grandchild : public child2,child1{ 
    public:
    Grandchild(){
        cout<<"Grandchild inherited from child1 and child2"<<endl;
    }
};
int main(int argc, char const *argv[])
{
    cout<<"Simple or single inheritance : "<<endl;
    parent1 obj;
     cout<<"\nHierarchial inheritance : "<<endl;
    parent1 obj1;
    parent2 obj2;
    cout<<"\nMultilevel inheritance : "<<endl;
    child1 obj3;
    cout<<"\nMultiple inheritance : "<<endl;
    child2 obj4;
    cout<<"\nHybrid inheritance : "<<endl;
    Grandchild obj5;
    return 0;
}