#include<iostream>
using namespace std;
class Base{
    public:
    int x = 1;
    protected :
    int y = 2;
    private:
    int z = 3;
};
class A : public Base{
    public:
    void pubDisplay(){
        cout<<"public inheritance:"<<endl;
        cout<<"x is public :"<<x<<endl;
        cout<<"y is protected :"<<y<<endl;
        cout<<"z is not accessible"<<endl;
        
    }
};
class B : protected Base{
    public:
    void proDisplay(){
        cout<<endl;
        cout<<"protected inheritance:"<<endl;
        cout<<"x is protected :"<<x<<endl;
        cout<<"y is private :"<<y<<endl;
        cout<<"z is not accessible"<<endl;
    }   
    

};
class C : private Base{
    public:
    public:
    void priDisplay(){
        cout<<endl;
        cout<<"private inheritance:"<<endl;
        cout<<"x is private :"<<x<<endl;
        cout<<"y is private :"<<y<<endl;
        cout<<"z is not accessible"<<endl;
        

    }
};
int main()
{
    A obj;
    obj.pubDisplay();
    B obj1;
    obj1.proDisplay();
    C obj2;
    obj2.priDisplay();
    return 0;
}