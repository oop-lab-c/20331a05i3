import java.util.*;
interface GrandParent
{
   public void display();
}
interface Parent1 extends GrandParent
{
    default void display()
    {
        System.out.println("Parent1 interface");
    }
}
interface Parent2 extends GrandParent
{
    default void display()
    {
        System.out.println("Parent2 Interface");
    }
}
class Child implements Parent1,Parent2 
{
    public void display()
    {
        System.out.println("child class");
        Parent1.super.display();
        Parent2.super.display();
    }
}
class MultipleInheriJava
{
    public  static void main(String[] args)
    {
        Child obj=new Child();
        obj.display();
    }
}
