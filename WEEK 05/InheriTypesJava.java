
// base class
class base{
    public static void main(String[] args) {
        System.out.println("Simple or single inheritance : ");
        parent1 obj1=new parent1();
        System.out.println("\nHierarchial inheritance : ");
        parent1 obj2=new parent1();
        parent2 obj3=new parent2();
        System.out.println("\nMultilevel inheritance : ");
        child1 obj4=new child1();
        System.out.println("\nHybrid inheritance : ");
        Grandchild obj6=new Grandchild();
    }
}
//single inheritance and Hierarchical Inheritance
class parent1 extends base{
    parent1(){
       System.out.println("parent1 is inherited from base class");
    }
};
class parent2 extends base{

    parent2(){ 
       System.out.println("parent2 is inherited from base class");
    }
};
//Multilevel Inheritance
class child1 extends parent1{
   
    child1(){
    System.out.println("child1 is inherited from parent1");
    }
}

//Hybrid inheritance
class Grandchild extends child1{ 
    Grandchild(){
      System.out.println("Grandchild inherited from child1");
    }
}
