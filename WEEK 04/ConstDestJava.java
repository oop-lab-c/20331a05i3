
public class Student {
    String fullName;
   int rollNum;
   double semPercentage;
   String collegeName;
   int collegeCode;
    
    Student(){
        fullName="lokesh";
        rollNum=32;
        semPercentage=85;
        collegeName="MVGR";
        collegeCode=33;        
    }
    void display(){
        System.out.println("Full name = "+fullName);
        System.out.println("Roll no = "+ rollNum);
        System.out.println("sem percentage = "+semPercentage);
        System.out.println("college code = "+collegeCode);
        System.out.println("college name = "+collegeName);
    }
    protected void finalize(){
        System.out.println("Object is destroyed");
    }

public static void main(String[] args) {
    Student obj = new Student();
    obj.display();
    obj = null;
    System.gc();
}
}