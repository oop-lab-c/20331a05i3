import java.util.*;
class Students{
    String fullName;
   double semPercentage;
   String collegeName;
   int collegeCode;
    Students(){
        collegeName = "MVGR";
        collegeCode = 33;
    }
    Students(String name,double sem){
        this();
        fullName= name;
        semPercentage = sem;
    }
    void display (){
        System.out.println("Full name = "+fullName);
        System.out.println("sem percentage = "+semPercentage);  
        System.out.println("college code = "+collegeCode);
        System.out.println("college name = "+collegeName);        
    }
    
    public static void main(String[] args) {
        double sem;
        String name;
        Scanner input = new Scanner(System.in);
        System.out.println("Enter the Name :");
        name = input.nextLine();
        System.out.println("Enter the sem precentage:" );
        sem = input.nextDouble();
        Students obj = new Students(name,sem);
        obj.display();

    }
}
