class Table{  
    synchronized void printTable(int n){
      for(int i=1;i<=3;i++){  
        System.out.println(n*i);
     
    }  
   }  
}
     
   class MyThread1 extends Thread{  
   Table x;  
   MyThread1(Table t){  
   x=t;  
   }  
   public void run(){  
   x.printTable(1);  
   }  
     
   }  
   class MyThread2 extends Thread{  
   Table y;  
   MyThread2(Table t){  
   y=t;  
   }  
   public void run(){  
   y.printTable(10);  
   }  
   }  
     
class TestSynchronization2{  
   public static void main(String args[]){  
   Table obj = new Table();  
   MyThread1 t1=new MyThread1(obj);  
   MyThread2 t2=new MyThread2(obj);  
   t1.start();  
   t2.start();  
   }  
   }  