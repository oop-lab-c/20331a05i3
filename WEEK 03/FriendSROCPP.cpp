#include<iostream>
using namespace std;
class box
{
    private:
        float length,width,height;
    public:
    box(float l, float b, float h){
        length=l;
        width=b;
        height=h;
    }
        void boxArea(float length,float width)
        {
            cout<<"The area of the box is : "<<length*width<<endl;
        }
        void boxVolume(float length, float width, float height) ;
        friend void displayBoxDimensions(box);
        void displayWelcomeMessage();
    
};
void box::boxVolume(float length,float width,float height)
{
    cout<<"The volume of the box is : "<<length*width*height<<endl;
}

void  displayBoxDimensions(box obj)
{
    
    cout<<"Lenght of box is "<<obj.length<<endl;
    cout<<"width of box is "<<obj.width<<endl;
    cout<<"Height of box is "<<obj.height<<endl;
}
 inline void box :: displayWelcomeMessage()
 {
    cout<<"welcome"<<endl;
}
int main()
{
     int l,b,h;
    cout<<"Enter length of the box : ";
    cin>>l;
    cout<<"Enter width of the box : ";
    cin>>b;
    cout<<"Enter height of the box : ";
    cin>>h;
    box obj(l,b,h);
    obj.displayWelcomeMessage();
    displayBoxDimensions(obj);
    obj.boxArea(l,b);
    obj.boxVolume(l,b,h);
    return 0;
}
